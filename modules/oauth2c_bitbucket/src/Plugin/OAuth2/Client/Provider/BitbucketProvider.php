<?php


namespace Drupal\oauth2c_bitbucket\Plugin\Oauth2\Client\Provider;

use Drupal\Core\Url;
use Drupal\oauth2c\ProviderBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;

/**
 * @OAuth2Provider(
 *   id = "bitbucket",
 *   inner_provider = "Stevenmaguire\OAuth2\Client\Provider\Bitbucket",
 *   collaborators = {
 *     "httpClient" = @HttpClient(
 *       base_uri = "https://bitbucket.com/site/oauth2"
 *     ),
 *     "httpApiClient" = @HttpClient(
 *       base_uri = "https://api.bitbucket.org/2.0"
 *     )
 *   },
 *   resource_owner = "bitbucket_owner"
 * )
 */
class BitbucketProvider extends ProviderBase {
  use BearerAuthorizationTrait;

  /**
   * @param \League\OAuth2\Client\Token\AccessToken $token
   * @return null|string
   */
  public function getName(AccessToken $token) {
    return $this->getResourceOwner($token)->getName();
  }

  /**
   * {@inheritdoc}
   * @return \Stevenmaguire\OAuth2\Client\Provider\BitbucketResourceOwner
   */
  public function getResourceOwner(AccessToken $token) {
    return parent::getResourceOwner($token);
  }

  /**
   * Get resource owner username
   *
   * @param \League\OAuth2\Client\Token\AccessToken $token
   * @return string|null
   */
  public function getUsername(AccessToken $token) {
    return $this->getResourceOwner($token)->getUsername();
  }

  /**
   * Get resource owner location
   *
   * @param \League\OAuth2\Client\Token\AccessToken $token
   * @return string|null
   */
  public function getLocation(AccessToken $token) {
    return $this->getResourceOwner($token)->getLocation();
  }

  public function getRepositories(AccessToken $token) {
    /** @var Client $client */
    $client = $this->getCollaborator('httpApiClient');
    $this->getAuthorizationHeaders($token);
    if ($token->hasExpired()) {
      $token = $this->refreshToken($token);
    }
    try {
//      $request = $this->getInnerProvider()->getAuthenticatedRequest('GET', 'https://api.bitbucket.org/2.0/repositories/' . $this->getResourceOwner($token)->getUsername(), $token);
      $url = $this->getResourceLinkUrl(
        $this->getResourceOwner($token)->toArray(),
        'repositories'
      )
        ->setOption('query', ['access_token' => $token]);
      $response = $client->get($url->toString());
//      $response = $client->send($request);

      $response_data = \json_decode($response->getBody()->getContents(), TRUE);

      $url = $this->getResourceLinkUrl($response_data->values[0], 'self');

      $response = $client->get($url->toString());

      $response_data = \json_decode($response->getBody()->getContents(), TRUE);

      return $response_data;
    } catch (GuzzleException $e) {
      return [
        'error' => [
          'code' => $e->getCode(),
          'file' => $e->getFile(),
          'line' => $e->getLine(),
          'message' => $e->getMessage(),
          'trace' => $e->getTrace(),
        ],
      ];
    }
  }

  /**
   * @param \stdClass|array $resource
   * @param string $link_name
   * @return \Drupal\Core\Url|null
   */
  public function getResourceLinkUrl($resource, $link_name) {
    $data = (array) $resource;
    $links = isset($data['links']) ? (array) $data['links'] : FALSE;
    if ($links && isset($links[$link_name])) {
      $link = (array) $links[$link_name];
      return Url::fromUri($link['href'])->setAbsolute(TRUE);
    }
    return NULL;
  }
}