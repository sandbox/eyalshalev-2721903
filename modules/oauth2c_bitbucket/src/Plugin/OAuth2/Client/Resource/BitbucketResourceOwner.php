<?php

namespace Drupal\oauth2c_bitbucket\Plugin\OAuth2\Client\Resource;

use Drupal\Component\Serialization\Json;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\Core\Url;
use Drupal\oauth2c\Plugin\DataType\ResourceData;
use Drupal\oauth2c\ResourceDefinition;
use Drupal\oauth2c\ResourceOwnerTypeBase;
use GuzzleHttp\Exception\GuzzleException;
use Stevenmaguire\OAuth2\Client\Provider\BitbucketResourceOwner as InternalBitbucketResourceOwner;

/**
 * @OAuth2ResourceType("bitbucket_owner")
 */
class BitbucketResourceOwner extends ResourceOwnerTypeBase {

  /**
   * {@inheritdoc}
   */
  public function doGetPropertiesDefinitions() {
    $definitions = parent::doGetPropertiesDefinitions();
    $definitions['name'] = DataDefinition::create('string');
    $definitions['username'] = DataDefinition::create('string');
    $definitions['location'] = DataDefinition::create('string');
    $definitions['links'] = MapDataDefinition::create();

    return $definitions;
  }

  public function getProperties(
    $resource,
    ResourceData $resource_property = NULL
  ) {
    if ($resource instanceof InternalBitbucketResourceOwner) {
      $definitions = $this->getPropertyDefinitions();
      $link_definition = $definitions['links'];
      if (isset($resource->toArray(
          )['links']) && $link_definition instanceof MapDataDefinition
      ) {
        foreach ($resource->toArray()['links'] as $link_id => $link_data) {
          $link_definition->setPropertyDefinition(
            $link_id,
            $this->getLinkDefinition($link_id)
          );
        }
      }

      $properties = parent::getProperties($resource, $resource_property);

      foreach ($properties['links'] as $resource_property) {
        if ($resource_property instanceof ResourceData) {
          $resource_property->setAccessToken(
            $resource_property->getAccessToken()
          );
        }
      }

      return $properties;
    }
    else {
      throw new \InvalidArgumentException(
        'The resource object of resource owner data must implement the ' . InternalBitbucketResourceOwner::class . ' interface.'
      );
    }
  }

  /**
   * Returns the data definition required for each link type.
   *
   * @param string $link_type
   * @return \Drupal\Core\TypedData\DataDefinitionInterface
   */
  protected function getLinkDefinition($link_type) {
    switch ($link_type) {
      case 'repositories':
        return ResourceDefinition::createFromDataType(
          'oauth2_resource:resource_pager'
        );
      default:
        // @TODO needs special handling
        return DataDefinition::create('any');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValues($resource, ResourceData $property = NULL) {
    if ($resource instanceof InternalBitbucketResourceOwner) {
      $values = [
        'name' => $resource->getName(),
        'username' => $resource->getUsername(),
        'location' => $resource->getLocation(),
      ];

      if (isset($resource->toArray(
          )['links']) && ($provider = $property->getProvider())
      ) {
        foreach ($resource->toArray()['links'] as $link_id => $link_data) {
          $values['links'][$link_id] = NULL;
          $url = Url::fromUri($link_data['href'])->setAbsolute(TRUE);
          $url->setOption(
            'query',
            [
              'access_token' => $property->getAccessToken()
                ->getToken(),
            ]
          );
          try {
            $response = \Drupal::httpClient()->get($url->toString());
            $values['links'][$link_id] = Json::decode($response->getBody());
          } catch (GuzzleException $e) {
            $message = 'Failed to get a resource from oauth2 server. Message: @message. Trace: @trace';
            $params = [
              '@message' => $e->getMessage(),
              '@trace' => $e->getTraceAsString(),
            ];
            \Drupal::logger('oauth2c_bitbucket.resource_owner')->error(
              $message,
              $params
            );
          }
        }
      }
      $values += parent::getValues($resource, $property);
      return $values;
    }
    else {
      throw new \InvalidArgumentException(
        'The resource object of resource owner data must implement the ' . InternalBitbucketResourceOwner::class . ' interface.'
      );
    }
  }
}