<?php

namespace Drupal\oauth2c_bitbucket\Plugin\OAuth2\Client\Resource;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\oauth2c\Plugin\DataType\ResourceData;
use Drupal\oauth2c\ResourceDefinition;
use Drupal\oauth2c\ResourceTypeBase;

/**
 * @OAuth2ResourceType("bitbucket_repository")
 */
class BitbucketRepository extends ResourceTypeBase {

  /**
   * @inheritDoc
   */
  public function getProperties($resource, ResourceData $resource_property) {
    $properties = [];
    foreach ($this->getPropertyDefinitions() as $property_name => $definition) {
      $properties[$property_name] = \Drupal::typedDataManager()
        ->createInstance(
          $definition->getDataType(),
          [
            'data_definition' => $definition,
            'name' => $property_name,
            'parent' => $resource_property,
          ]
        );
      switch ($property_name) {
        case 'links':
          $links = $resource[$property_name];
          $link_definition = $definition['links'];

          if ($link_definition instanceof MapDataDefinition) {
            foreach ($links as $link_type => $link_data) {
              // @TODO needs special handling
              $link_definition->setPropertyDefinition(
                $link_type,
                DataDefinition::create('any')
              );
            }
          }

          $properties[$property_name]->setValue($links);
          break;
        default:
          $properties[$property_name]->setValue($resource[$property_name]);
      }
    }

    return $properties;
  }

  /**
   * @inheritDoc
   */
  protected function doGetPropertiesDefinitions() {
    $definitions = [
      'scm' => DataDefinition::create('string'),
      'website' => DataDefinition::create('string'),
      'has_wiki' => DataDefinition::create('boolean'),
      'name' => DataDefinition::create('string'),
      'links' => MapDataDefinition::create(),
      'fork_policy' => DataDefinition::create('string'),
      'uuid' => DataDefinition::create('string'),
      'language' => DataDefinition::create('string'),
      'created_on' => DataDefinition::create('string'),
      'full_name' => DataDefinition::create('string'),
      'has_issues' => DataDefinition::create('boolean'),
      'owner' => ResourceDefinition::createFromDataType(
        'oauth2_resource:bitbucket_owner'
      ),
      'updated_on' => DataDefinition::create('timestamp'),
      'size' => DataDefinition::create('integer'),
      'type' => DataDefinition::create('string'),
      'is_private' => DataDefinition::create('boolean'),
      'description' => DataDefinition::create('string'),
    ];

//    $definitions['links']->getItemDefinition()->setResourceTypeId('resource_pager');
    return $definitions;
  }

}