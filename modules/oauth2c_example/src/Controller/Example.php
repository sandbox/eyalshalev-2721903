<?php


namespace Drupal\oauth2c_example\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Example extends ControllerBase {

  public function authenticate() {

    if ($state_key = \Drupal::requestStack()->getCurrentRequest()->get(
      'state'
    )
    ) {

      $state_data = \Drupal::keyValueExpirable('oauth2_client_provider_state')
        ->get($state_key);

      /** @var Url $url */
      $url = $state_data['return_url'];

      $url->setRouteParameter(
        'code',
        \Drupal::requestStack()->getCurrentRequest()->get('code')
      );

      $url->setRouteParameter(
        'state',
        \Drupal::requestStack()->getCurrentRequest()->get('state')
      );

      return new RedirectResponse($url->toString());
    }
    return [
      'error' => [
        '#markup' => 'ERROR: returned state does not exist.',
      ],
    ] + $this->oauth2c();
  }

  public function oauth2c() {

    return [
      'token' => [
        '#type' => 'oauth2_client_token',
        '#provider_id' => 'generic.example',
        '#return_url' => Url::fromRouteMatch(\Drupal::routeMatch()),
      ],
    ];
  }

}