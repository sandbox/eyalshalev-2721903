<?php


namespace Drupal\oauth2c\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Url;
use Drupal\oauth2c\ProviderEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Redirect extends ControllerBase {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $keyValueAccessState;

  public function __construct(KeyValueStoreExpirableInterface $key_value_store) {
    $this->keyValueAccessState = $key_value_store;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('oauth2.key_value.access_state')
    );
  }

  /**
   * @param string $state
   * @param string $code
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function accessCode($state, $code) {

    if ($state_data = $this->keyValueAccessState->get($state)) {
      /** @var Url $url */
      $url = $state_data['return_url'];

      $url->setRouteParameter('code', $code);

      $url->setRouteParameter('state', $state);

      return new RedirectResponse($url->toString());
    }
    else {
      throw new \LogicException("The received state_key ({$state}) has expired or is not the one sent.");
    }
  }
}