<?php


namespace Drupal\oauth2c\Routing\Enhancer;


use Drupal\Core\Routing\Enhancer\RouteEnhancerInterface;
use Drupal\oauth2c\Controller\Redirect;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

class OAuth2RedirectRouteEnhancer implements RouteEnhancerInterface {

  /**
   * Declares if the route enhancer applies to the given route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *  The route to consider attaching to.
   *
   * @return bool
   *   TRUE if the check applies to the passed route, False otherwise.
   */
  public function applies(Route $route) {
    if ($route->hasDefault('_controller')) {
      $controller = explode('::', $route->getDefault('_controller'));
      return isset($controller[0]) && $controller[0] == '\\' .Redirect::class;
    }
    return FALSE;
  }

  /**
   * Update the defaults based on its own data and the request.
   *
   * @param array $defaults the getRouteDefaults array.
   * @param Request $request the Request instance.
   *
   * @return array the modified defaults. Each enhancer MUST return the
   *               $defaults but may add or remove values.
   */
  public function enhance(array $defaults, Request $request) {
    $defaults['code'] = $request->query->get('code', $defaults['code']);
    $defaults['state'] = $request->query->get('state', $defaults['state']);
    return $defaults;
  }
}