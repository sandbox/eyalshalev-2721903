<?php

namespace Drupal\oauth2c;

use Drupal\Component\Plugin\Discovery\DiscoveryCachedTrait;
use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OAuth2ClientGrantManager.
 *
 * @package Drupal\oauth2c
 */
class GrantManager extends PluginManagerBase implements GrantManagerInterface, ContainerInjectionInterface {

  use DiscoveryCachedTrait;
  use UseCacheBackendTrait;
  use ContainerAwareTrait;

  const SERVICE_NAME = 'oauth2.client.grant.manager';

  protected $namespaces;
  
  protected $moduleHandler;

  protected $discovery;

  /**
   * OAuth2ClientGrantManager constructor.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->moduleHandler = $module_handler;
    $this->discovery = new YamlDiscovery('oauth2.client.grants', $module_handler->getModuleDirectories());
    $this->cacheBackend = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
      $container->get('module_handler'),
      $container->get('cache.oauth2.client.grant')
    );
    $instance->setContainer($container);
    return $instance;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|NULL $container
   * @return static
   */
  public static function getService(ContainerInterface $container = NULL) {
    $container = $container ?: \Drupal::getContainer();
    return $container->get(static::SERVICE_NAME);
  }

  /**
   * Returns all available grants keyed by the grant id.
   *
   * @return \League\OAuth2\Client\Grant\AbstractGrant[]
   */
  public function getAllGrants() {
    $grants = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      $grants[$id] = $this->getGrant($id);
    }
    return $grants;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = $this->getCachedDefinitions();
    if (!isset($definitions)) {
      $definitions = $this->getDiscovery()->getDefinitions();
      $this->setCachedDefinitions($definitions);
    }
    return $definitions;
  }

  /**
   * Returns the cached plugin definitions of the decorated discovery class.
   *
   * @return array|null
   *   On success this will return an array of plugin definitions. On failure
   *   this should return NULL, indicating to other methods that this has not
   *   yet been defined. Success with no values should return as an empty array
   *   and would actually be returned by the getDefinitions() method.
   */
  protected function getCachedDefinitions() {
//    if (!isset($this->definitions) && $cache = $this->cacheGet($this->cacheKey)) {
//      $this->definitions = $cache->data;
//    }
    return $this->definitions;
  }

  /**
   * Sets a cache of plugin definitions for the decorated discovery class.
   *
   * @param array $definitions
   *   List of definitions to store in cache.
   */
  protected function setCachedDefinitions($definitions) {
//    $this->cacheSet($this->cacheKey, $definitions, Cache::PERMANENT, $this->cacheTags);
    $this->definitions = $definitions;
  }

  /**
   * A grant object that represents this grant id.
   *
   * @param string $id
   * @return \League\OAuth2\Client\Grant\AbstractGrant
   */
  public function getGrant($id) {
    if (!($grantCache = $this->cacheGet($id))) {
      $grant = $this->generateGrantFromDefinition($id, $this->getDefinition($id));

      $this->cacheSet($id, $grant, Cache::PERMANENT, $this->generateCacheTags($id));
    }
    else {
      $grant = $grantCache->data;
    }

    return $grant;
  }

  /**
   * @param string $id
   * @param array $definition
   * @return \League\OAuth2\Client\Grant\AbstractGrant
   */
  protected function generateGrantFromDefinition($id, $definition) {
    assert('array_key_exists(\'class\', $definition)', 'Grant definition must contain a class property.');
    assert('class_exists($definition[\'class\'])', 'The grant class property must be a valid class');
    assert('is_subclass_of($definition[\'class\'], \'\League\OAuth2\Client\Grant\AbstractGrant\')', 'The grant class must inherit AbstractGrant.');

    $grantClassReflection = new \ReflectionClass($definition['class']);
    if ($grantClassReflection->isSubclassOf(ContainerInjectionInterface::class)) {
      $createGrantMethod = $grantClassReflection->getMethod('create');
      $grant = $createGrantMethod->invoke(NULL, $this->getContainer());
    }
    elseif ($grantClassReflection->isSubclassOf(ContainerFactoryPluginInterface::class)) {
      $createGrantMethod = $grantClassReflection->getMethod('create');
      $grant = $createGrantMethod->invokeArgs(NULL, [
        $this->getContainer(),
        [],
        $id,
        $definition
      ]);
    }
    else {
      $grant = $grantClassReflection->newInstance();
    }

    return $grant;
  }

  public function getContainer() {
    if (empty($this->container)) {
      $this->setContainer(\Drupal::getContainer());
    }
    return $this->container;
  }

  protected function generateCacheTags($id) {
    return [
      'oauth2.client',
      'oauth2.client.grant',
      'oauth2.client.grant:' . $id
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function useCaches($use_caches = FALSE) {
    $this->useCaches = $use_caches;
    if (!$use_caches) {
      $this->definitions = NULL;
    }
  }
}
