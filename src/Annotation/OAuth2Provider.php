<?php


namespace Drupal\oauth2c\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class OAuth2Provider extends Plugin {

  /**
   * The class that this annotation is attached to.
   *
   * @see \Drupal\Component\Annotation\AnnotationInterface::getClass
   * @var string
   */
  public $class;

  /**
   * The plugin identifier string.
   *
   * @see \Drupal\Component\Annotation\AnnotationInterface::getId
   * @var string
   */
  public $id;

  /**
   * An array of collaborators that may be used to override this provider's
   * default behavior.
   * Collaborators include:
   *   - grantFactory: \League\OAuth2\Client\Grant\GrantFactory
   *   - requestFactory: \League\OAuth2\Client\Tool\RequestFactory
   *   - httpClient: \GuzzleHttp\HttpClient
   *   - randomFactory: \SecurityLib\Strength\Factory
   * Individual providers may introduce more collaborators, as needed.
   *
   * @see \League\OAuth2\Client\Provider\AbstractProvider::__construct
   * @var array
   */
  public $collaborators = [];

  public $leagueProviderClass;

  public $resource_owner;

  public function __construct($values) {
    if (empty($values['collaborators']['httpClient'])) {
      $values['collaborators']['httpClient'] = (new HttpClient([]))->get();
    }
    parent::__construct($values);
  }
}