<?php


namespace Drupal\oauth2c\Annotation;


use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class HttpClient extends Plugin {

  /**
   * @return \GuzzleHttp\Client
   */
  public function get() {
    return \Drupal::service('http_client_factory')->fromOptions($this->definition);
  }
}