<?php


namespace Drupal\oauth2c\Annotation;


use Drupal\Component\Annotation\PluginID;

/**
 * @Annotation
 */
class OAuth2ResourceType extends PluginID {
}