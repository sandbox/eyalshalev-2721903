<?php


namespace Drupal\oauth2c\Entity;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\oauth2c\ProviderEntityInterface;

/**
 * @ConfigEntityType(
 *   id = "oauth2_provider",
 *   label = @Translation("OAuth2 Client provider config"),
 *   group_label = @Translation("OAuth2 Client provider configurations"),
 *   description = @Translation("OAuth2 Client provider configuration entity."),
 *   admin_permission = "administer oauth2 client provider configs",
 *   config_prefix = "provider",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "provider_name"
 *   },
 *   handlers = {
 *     "list_builder" = "Drupal\oauth2c\ProviderEntityListBuilder",
 *     "form" = {
 *       "default" = "Drupal\oauth2c\Form\ProviderEntityForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\oauth2c\Entity\ProviderEntityRouteProvider",
 *     },
 *   },
 *   links = {
 *     "collection" = "/admin/structure/oauth2/client/provider",
 *     "edit-form" = "/admin/structure/oauth2/client/provider/{oauth2_provider}",
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "plugin_id",
 *     "provider_name",
 *     "options",
 *   }
 * )
 */
class ProviderEntity extends ConfigEntityBase implements ProviderEntityInterface, EntityWithPluginCollectionInterface {

  use StringTranslationTrait;

  protected $plugin_id;

  protected $provider_name;

  protected $id;

  protected $options = [];

  protected $pluginCollections = [];

  public function __construct(array $values, $entity_type) {
    $values += [
      'provider_name' => 'default',
      'options' => []
    ];
    $values['options'] += [
      'redirectUri' => Url::fromRoute('oauth2c.redirect.access_code')->toString()
    ];
    parent::__construct($values, $entity_type);
  }

  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }

  public function toUrl($rel = 'edit-form', array $options = []) {
    if ($rel === 'authorize') {
      $options += [
        'state' => []
      ];
      $options['state'] += [
        'provider_id' => $this->id(),
        'return_url' => Url::fromRouteMatch(\Drupal::routeMatch())
      ];
      $state = $this->createState($options['state']);
      return $this->getProvider()
        ->getAuthorizationUrl($state)
        ->setOptions($options);
    }
    return parent::toUrl($rel, $options);
  }

  public function id() {
    return implode('.', [$this->plugin_id, $this->provider_name]);
  }

  public function createState(array $data) {
    $state_key = Crypt::hmacBase64(serialize($data), Settings::getHashSalt());
    $key_value_storage = \Drupal::service('oauth2.key_value.access_state');
    if (empty($key_value_storage->get($state_key))) {
      $key_value_storage->setWithExpire($state_key, $data, 3600);
    }
    return $state_key;
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return $this->getPluginCollections()['options']->get($this->plugin_id);
  }

  /**
   * Gets the plugin collections used by this entity.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection[]
   *   An array of plugin collections, keyed by the property name they use to
   *   store their configuration.
   */
  public function getPluginCollections() {
    if (empty($this->pluginCollections)) {
      $manager = \Drupal::service('oauth2.client.provider.manager');
      $this->pluginCollections = [
        'options' => new DefaultSingleLazyPluginCollection($manager, $this->plugin_id, $this->options)
      ];
    }

    return $this->pluginCollections;
  }
}