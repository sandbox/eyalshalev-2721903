<?php


namespace Drupal\oauth2c\Entity;


use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Routing\Route;

class ProviderEntityRouteProvider extends AdminHtmlRouteProvider {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($edit_route = $this->getCollectionRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.collection", $edit_route);
    }

    return $collection;
  }

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @return \Symfony\Component\Routing\Route|null
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('collection') && $entity_type->hasListBuilderClass()) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('collection'));
      $route
        ->addDefaults([
          '_entity_list' => $entity_type_id,
          '_title' => (string) $entity_type->getGroupLabel(),
        ])
//        ->setRequirement('_entity_access', "{$entity_type_id}.view");
        ->setRequirement('_access', 'TRUE');

      return $route;
    }
    return NULL;
  }

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @return \Symfony\Component\Routing\Route|null
   */
  protected function getAuthenticationRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('authenticate') && $entity_type->has) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('collection'));
      $route
        ->addDefaults([
          '_entity_list' => $entity_type_id,
          '_title' => (string) $entity_type->getGroupLabel(),
        ])
//        ->setRequirement('_entity_access', "{$entity_type_id}.view");
        ->setRequirement('_access', 'TRUE');

      return $route;
    }
    return NULL;
  }
}