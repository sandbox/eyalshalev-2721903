<?php

namespace Drupal\oauth2c;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface OAuth2ClientProviderManagerInterface.
 *
 * @package Drupal\oauth2c
 */
interface ProviderManagerInterface extends PluginManagerInterface {

  /**
   * @param string $provider_id
   *   The id of the provider entity.
   * @return \Drupal\oauth2c\ProviderInterface
   */
  public function getProvider($provider_id);
}
