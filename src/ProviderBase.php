<?php

namespace Drupal\oauth2c;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use League\OAuth2\Client\Grant\AbstractGrant;
use League\OAuth2\Client\Grant\Exception\InvalidGrantException;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;

/**
 * Base class for OAuth2 client provider plugins
 *
 * @see \Drupal\oauth2c\OAuth2ClientProviderManager
 */
abstract class ProviderBase extends PluginBase implements ProviderInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\oauth2c\GrantManagerInterface
   */
  protected static $grantManager;
  /**
   * @var \League\OAuth2\Client\Provider\ResourceOwnerInterface
   */
  protected $resourceOwner;
  /**
   * The internal League OAuth2 client provider object.
   *
   * @var \League\OAuth2\Client\Provider\AbstractProvider
   */
  protected $innerProvider;

  /**
   * Constructs a new OAuth2 client provider plugin object.
   *
   * @param array $configuration
   *   The configuration (options) array passed to the internal League client.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    assert('$this->isInnerProviderClassValid()', 'Is the league provider class valid?');
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationUrl($state) {
    $options = [
      'state' => $state,
      'scopes' => $this->getConfigurationValue('scopes') ?: [],
      'response_type' => $this->getConfigurationValue('response_type') ?: static::DEFAULT_RESPONSE_TYPE,
      'approval_prompt' => $this->getConfigurationValue('approval_prompt') ?: static::DEFAULT_APPROVAL_PROMPT
    ];
    $url_string = $this->getInnerProvider()->getAuthorizationUrl($options);
    return Url::fromUri($url_string, ['absolute' => TRUE]);
  }

  /**
   * Gets this plugin's configuration.
   *
   * @param string|array $key
   *   The key(s) to use to drill down the configuration array.
   *
   * @see \Drupal\Component\Utility\NestedArray::getValue()
   *
   * @return mixed
   */
  public function getConfigurationValue($key) {
    $keys = is_array($key) ? $key : [$key];
    $key_exists = FALSE;
    $configuration = $this->getConfiguration();
    $value = NestedArray::getValue($configuration, $keys, $key_exists);
    return $key_exists ? $value : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return NestedArray::mergeDeep($this->defaultConfiguration(), $this->configuration);
  }

  public function defaultConfiguration() {
    return [];
  }

  /**
   * The internal league OAuth2 client provider.
   * @param string $state
   * @param bool $forceNew
   *   Will create a new league provider object even if one exists.
   * @return \League\OAuth2\Client\Provider\AbstractProvider
   */
  protected function getInnerProvider($state = NULL, $forceNew = FALSE) {
    if ($forceNew || empty($this->innerProvider)) {
      $league_provider_class = new \ReflectionClass($this->getInnerProviderClass());
      $options = $this->getConfiguration();
      $options['state'] = $state;

      $this->innerProvider = $league_provider_class->newInstanceArgs([
        $options,
        $this->getCollaborators()
      ]);
    }

    return $this->innerProvider;
  }

  /**
   * @return string
   *   The class name (and namespace) of a provider class.
   */
  protected function getInnerProviderClass() {
    return $this->getPluginDefinition()['inner_provider'];
  }

  /**
   * The collaborators array provided by the plugin definition.
   *
   * @return array
   */
  protected function getCollaborators() {
    return $this->getPluginDefinition()['collaborators'];
  }

  /**
   * @param string $code
   *   The authorization code returned by the oauth2 server.
   * @param bool $throw
   *   If FALSE then the expected exceptions will be catched and logged.
   * @return \League\OAuth2\Client\Token\AccessToken|null
   *   An access token if the code is valid.
   *   NULL if an exception was handled and $throw is set to FALSE
   *
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   * @throws \League\OAuth2\Client\Grant\Exception\InvalidGrantException
   */
  public function getAccessToken($code, $throw = FALSE) {
    $options = [
      'code' => $code
    ];
    $authorization_code = $this->getGrant('authorization_code');
    return $this->doGetAuthorizationToken($authorization_code, $options, $throw);
  }

  /**
   * @param string $grant_name
   * @return \League\OAuth2\Client\Grant\AbstractGrant
   */
  protected function getGrant($grant_name) {
    return static::getGrantManager()->getGrant($grant_name);
  }

  /**
   * @return \Drupal\oauth2c\GrantManagerInterface
   */
  protected static function getGrantManager() {
    if (empty(static::$grantManager)) {
      static::$grantManager = \Drupal::service('oauth2.client.grant.manager');
    }
    return static::$grantManager;
  }

  /**
   * @param \League\OAuth2\Client\Grant\AbstractGrant $grant
   * @param array $options
   * @param bool $throw
   * @return \League\OAuth2\Client\Token\AccessToken|null
   * @throws \League\OAuth2\Client\Grant\Exception\InvalidGrantException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  protected function doGetAuthorizationToken(AbstractGrant $grant, array $options = [], $throw = FALSE) {
    try {
      return $this->getInnerProvider()->getAccessToken($grant, $options);
    }
    catch (InvalidGrantException $e) {}
    catch (IdentityProviderException $e) {}

    if ($throw) {
      throw $e;
    }
    else {
      $this->logger()->error($e->getMessage());
    }
    return NULL;
  }

  /**
   * A logger with the service id as the logger channel name.
   * @return \Psr\Log\LoggerInterface
   */
  protected function logger() {
    return \Drupal::logger($this->serviceId());
  }

  /**
   * Generates a string id to represent this plugin instance across services.
   * @return string
   */
  protected function serviceId() {
    return implode('.', ['oauth2c', 'client', 'provider', $this->pluginId]);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceOwner(AccessToken $token) {
    // Cache the resource owner locally.
    // Requesting a resource owner requires an http call.
    // @TODO consider using caching service.
    if (empty($this->resourceOwner)) {
      $this->resourceOwner = $this->getInnerProvider()
        ->getResourceOwner($token);
    }
    return $this->resourceOwner;
  }

  public function setState($state) {
    $this->getInnerProvider($state, TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [
      'module' => ['oauth2c']
    ];
  }

  /**
   * Sets the configuration for this plugin instance.
   *
   * @param string|array $key
   *   The key(s) to use to drill down the configuration array.
   *   If null then the entire configuration array will be replaced.
   *
   * @param mixed $value
   */
  public function setConfigurationValue($key, $value) {
    $keys = is_array($key) ? $key : [$key];
    NestedArray::setValue($this->configuration, $keys, $value);
  }

  /**
   * Checks if the configuration array contains a key(s).
   *
   * @param string|array $key
   *   The key(s) to use to drill down the configuration array.
   * @return bool
   */
  public function hasConfigurationValue($key) {
    $keys = is_array($key) ? $key : [$key];
    $key_exists = NestedArray::keyExists($this->configuration, $keys);
    return $key_exists;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Options'),
      '#tree' => TRUE
    ];
    $form['options']['clientId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client id'),
      '#default_value' => $this->getConfigurationValue('clientId'),
      '#required' => TRUE
    ];
    $form['options']['clientSecret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#default_value' => $this->getConfigurationValue('clientSecret'),
      '#required' => TRUE
    ];
    $form['options']['redirectUri'] = [
      '#type' => 'path',
      '#title' => $this->t('Redirect url'),
      '#description' => $this->t('The internal url that the user will be redirected to after authorization.'),
      '#default_value' => $this->getConfigurationValue('redirectUri'),
      '#required' => TRUE
    ];
    $form['options']['urlAuthorize'] = [
      '#type' => 'url',
      '#title' => $this->t('Authorization url'),
      '#default_value' => $this->getConfigurationValue('urlAuthorize'),
      '#required' => TRUE
    ];
    $form['options']['urlAccessToken'] = [
      '#type' => 'url',
      '#title' => $this->t('Access token url'),
      '#default_value' => $this->getConfigurationValue('urlAccessToken'),
      '#required' => TRUE
    ];
    $form['options']['urlResourceOwnerDetails'] = [
      '#type' => 'url',
      '#title' => $this->t('Resource owner details url'),
      '#default_value' => $this->getConfigurationValue('urlResourceOwnerDetails'),
      '#required' => TRUE
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement validateConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValue('options', $this->defaultConfiguration()));
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function refreshToken(AccessToken $token) {
    return $this->getInnerProvider()->getAccessToken(GrantManager::getService()
      ->getGrant('refresh_token'), [
      'refresh_token' => $token->getRefreshToken()
    ]);
  }

  /**
   * Validates the league provider class.
   *
   * @return bool
   */
  protected function isInnerProviderClassValid() {
    if (empty($this->getPluginDefinition()['inner_provider'])) {
      return FALSE;
    }
    $class = $this->getPluginDefinition()['inner_provider'];
    return is_string($class) && class_exists($class)
    && is_subclass_of($class, AbstractProvider::class);
  }

  protected function getCollaborator($collaborator) {
    return $this->getPluginDefinition()['collaborators'][$collaborator];
  }
}