<?php

namespace Drupal\oauth2c;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use League\OAuth2\Client\Token\AccessToken;

/**
 * This plugin interface represents a service provider against an OAuth 2, 
 * authorization server.
 *
 * Glossary:
 *  - Server: The OAuth 2 authorization server.
 *  - Client:
 * 
 * The flow is as follows:
 * 1. The client  
 */
interface ProviderInterface extends ConfigurablePluginInterface, PluginFormInterface, PluginInspectionInterface, DerivativeInspectionInterface {

  const RESPONSE_TYPE_CODE = 'code';
  const RESPONSE_TYPE_TOKEN = 'token';

  const APPROVAL_PROMPT_AUTO = 'auto';
  const APPROVAL_PROMPT_FORCE = 'force';

  const DEFAULT_RESPONSE_TYPE = self::RESPONSE_TYPE_CODE;
  const DEFAULT_APPROVAL_PROMPT = self::APPROVAL_PROMPT_AUTO;


  /**
   * Builds the authorization URL.
   *
   * @link https://tools.ietf.org/html/rfc6749#section-3.1.1
   *
   * @param string $state
   *   The state variable is used to validate the returned code.
   *
   * @return \Drupal\Core\Url
   *   Authorization URL
   */
  public function getAuthorizationUrl($state);

  /**
   * Completes the authentication process against the OAuth 2 server.
   * This method will use the $code argument to create and save an AccessToken
   * object.
   *
   * If the authentication process fails then the saved state variable is reset.
   *
   * @param string $code
   *   The code that the OAuth 2 server returned.
   * @return \League\OAuth2\Client\Token\AccessToken
   *
   * @see \Drupal\oauth2c\ServiceProviderInterface::isValidState()
   * @see \Drupal\oauth2c\ServiceProviderInterface::forgetState()
   */
  public function getAccessToken($code);

  /**
   * @param \League\OAuth2\Client\Token\AccessToken $token
   * @return \League\OAuth2\Client\Provider\ResourceOwnerInterface
   */
  public function getResourceOwner(AccessToken $token);

  /**
   * @param string $state
   * @return static
   */
  public function setState($state);

  /**
   * @param \League\OAuth2\Client\Token\AccessToken $token
   * @return \League\OAuth2\Client\Token\AccessToken
   */
  public function refreshToken(AccessToken $token);
}