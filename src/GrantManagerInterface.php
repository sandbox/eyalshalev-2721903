<?php

namespace Drupal\oauth2c;

/**
 * Interface OAuth2ClientGrantManagerInterface.
 *
 * @package Drupal\oauth2c
 */
interface GrantManagerInterface {

  /**
   * A grant object that represents this grant id.
   *
   * @param string $id
   * @return \League\OAuth2\Client\Grant\AbstractGrant
   */
  public function getGrant($id);

  /**
   * Returns all available grants keyed by the grant id.
   *
   * @return \League\OAuth2\Client\Grant\AbstractGrant[]
   */
  public function getAllGrants();

}
