<?php

namespace Drupal\oauth2c\Plugin\OAuth2\Client\Provider;

use Drupal\oauth2c\ProviderBase;

/**
 * @OAuth2Provider(
 *   id = "generic",
 *   inner_provider = "\League\OAuth2\Client\Provider\GenericProvider",
 *   resource_owner = "generic_owner"
 * )
 */
class GenericProvider extends ProviderBase {
}