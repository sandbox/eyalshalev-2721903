<?php


namespace Drupal\oauth2c\Plugin\OAuth2\Client\Resource;


use Drupal\oauth2c\Plugin\DataType\ResourceData;
use Drupal\oauth2c\ResourceTypeBase;

/**
 * @OAuth2ResourceType("empty_resource")
 */
class EmptyResource extends ResourceTypeBase {

  /**
   * @inheritDoc
   */
  public function getProperties($resource, ResourceData $resource_property) {
    return [];
  }

  /**
   * @inheritDoc
   */
  protected function doGetPropertiesDefinitions() {
    return [];
  }

}