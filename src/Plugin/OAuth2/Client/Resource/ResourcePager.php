<?php


namespace Drupal\oauth2c\Plugin\OAuth2\Client\Resource;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\Core\TypedData\Plugin\DataType\IntegerData;
use Drupal\Core\TypedData\Plugin\DataType\ItemList;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\oauth2c\Plugin\DataType\ResourceData;
use Drupal\oauth2c\ResourceTypeBase;

/**
 * @OAuth2ResourceType("resource_pager")
 */
class ResourcePager extends ResourceTypeBase {
  /**
   * @inheritDoc
   */
  public function getProperties($resource, ResourceData $resource_property) {
    $definitions = $this->getPropertyDefinitions();
    /** @var TypedDataInterface[] $properties */
    $properties = [
      'pagelen' => IntegerData::createInstance($definitions['pagelen'], 'pagelen', $resource_property),
      'page' => IntegerData::createInstance($definitions['page'], 'page', $resource_property),
      'size' => IntegerData::createInstance($definitions['size'], 'size', $resource_property)
    ];

    $properties['values'] = ItemList::createInstance($definitions['values'], 'values', $resource_property);

    foreach ($properties as $prop_name => $property) {
      $property->setValue($resource[$prop_name]);
    }

    return $properties;
  }

  /**
   * @inheritDoc
   */
  protected function doGetPropertiesDefinitions() {
    $definitions = [
      'pagelen' => DataDefinition::create('integer'),
      'page' => DataDefinition::create('integer'),
      'size' => DataDefinition::create('integer')
    ];


    $definitions['values'] = ListDataDefinition::create('oauth2_resource');
    $definitions['values']->getItemDefinition()
      ->setResourceTypeId('bitbucket_repository');

    return $definitions;
  }

}