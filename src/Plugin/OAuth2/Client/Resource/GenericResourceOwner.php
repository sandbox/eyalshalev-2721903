<?php


namespace Drupal\oauth2c\Plugin\OAuth2\Client\Resource;


use Drupal\oauth2c\ResourceOwnerTypeBase;

/**
 * @OAuth2ResourceType("generic_owner")
 */
class GenericResourceOwner extends ResourceOwnerTypeBase {

}