<?php


namespace Drupal\oauth2c\Plugin\DataType;

use Drupal\Core\TypedData\TypedData;
use Drupal\oauth2c\ProviderManager;

/**
 * The OAuth2 access token type.
 *
 * The AccessToken data.
 *
 * @DataType(
 *   id = "oauth2_access_token",
 *   label = @Translation("OAuth2 access token"),
 *   constraints = {
 *     "ValidAccessToken" = {}
 *   }
 * )
 */
class AccessTokenData extends TypedData {

  /**
   * @var \Drupal\oauth2c\ProviderInterface
   */
  protected $provider;

  /**
   * The serialized version of an AccessToken object.
   * @var \League\OAuth2\Client\Token\AccessToken
   */
  protected $value;

  /**
   * Has the access token expired and does it need refreshing.
   *
   * @return bool
   */
  public function hasExpired() {
    return $this->getValue()->hasExpired();
  }

  /**
   * @return \League\OAuth2\Client\Token\AccessToken
   */
  public function getValue() {
    return parent::getValue();
  }

  /**
   * Refreshes the access token and sets the new token as this value.
   * @param bool $notify
   *   Should the parent typed data be notified for this change?
   * @return $this
   */
  public function refreshToken($notify = TRUE) {
    $this->setValue($this->getProvider()
      ->refreshToken($this->getValue()), $notify);
    return $this;
  }

  /**
   * The OAuth2 client provider plugin.
   *
   * @return \Drupal\oauth2c\ProviderInterface|null
   */
  public function getProvider() {
    if (empty($this->provider) && $provider_id = $this->getDataDefinition()
        ->getSetting('provider_id')
    ) {
      $this->provider = ProviderManager::getService()
        ->getProvider($provider_id);
    }
    return $this->provider;
  }

  /**
   * {@inheritdoc}
   */
  public function getString() {
    return $this->getValue()->getToken();
  }
}