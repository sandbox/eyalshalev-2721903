<?php


namespace Drupal\oauth2c\Plugin\DataType;


use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\TypedData;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\oauth2c\ProviderManager;
use League\OAuth2\Client\Token\AccessToken;

/**
 * @DataType(
 *   id = "oauth2_resource",
 *   label = @Translation("Resource owner"),
 *   definition_class = "\Drupal\oauth2c\ResourceDefinition"
 * )
 */
class ResourceData extends TypedData implements \IteratorAggregate, ComplexDataInterface {

  /**
   * @var \Drupal\oauth2c\ProviderInterface
   */
  protected $provider;

  /**
   * @var \League\OAuth2\Client\Provider\ResourceOwnerInterface
   */
  protected $value;

  /**
   * @var \Drupal\Core\TypedData\TypedDataInterface[]
   */
  protected $properties;

  /**
   * @var \League\OAuth2\Client\Token\AccessToken
   */
  protected $accessToken;

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {
    return $this->getProperties()[$property_name];
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties($include_computed = TRUE) {
    if (empty($this->properties)) {
      $this->properties = $this->getDataDefinition()
        ->getProperties($this->value, $this);
    }

    return $this->properties;
  }

  /**
   * @return \Drupal\oauth2c\ResourceDefinition
   */
  public function getDataDefinition() {
    return parent::getDataDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value, $notify = TRUE) {
    \Drupal::logger('oauth2c.resource')
      ->warning('Resource items are immutable.');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return array_map(function (TypedDataInterface $property) {
      return $property instanceof ComplexDataInterface ? $property->toArray() : $property->getValue();
    }, $this->getProperties());
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->value);
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($name) {
    $this->getParent()->onChange($this->getName());
  }

  /**
   * @inheritDoc
   */
  public function getIterator() {
    return new \ArrayIterator($this->getProperties());
  }

  /**
   * Get the access token set on this resource.
   * Will return null if none was set.
   * @return \League\OAuth2\Client\Token\AccessToken|null
   */
  public function getAccessToken() {
    return $this->accessToken;
  }

  /**
   * Use this method to set the access token on this property.
   * @param \League\OAuth2\Client\Token\AccessToken $access_token
   * @return static
   */
  public function setAccessToken(AccessToken $access_token) {
    $this->accessToken = $access_token;
    return $this;
  }

  /**
   * The OAuth2 client provider plugin.
   *
   * @return \Drupal\oauth2c\ProviderInterface|null
   */
  public function getProvider() {
    if (empty($this->provider) && $provider_id = $this->getDataDefinition()
        ->getSetting('provider_id')
    ) {
      $this->provider = static::doGetProvider($provider_id);
    }
    return $this->provider;
  }

  /**
   * The OAuth2 client provider plugin.
   *
   * @param string $provider_id
   * @return \Drupal\oauth2c\ProviderInterface|null
   */
  protected static function doGetProvider($provider_id) {
    return ProviderManager::getService()->getProvider($provider_id);
  }

}