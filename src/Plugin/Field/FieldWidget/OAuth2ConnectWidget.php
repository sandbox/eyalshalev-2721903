<?php


namespace Drupal\oauth2c\Plugin\Field\FieldWidget;


use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\oauth2c\Entity\ProviderEntity;
use Drupal\oauth2c\ProviderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @FieldWidget(
 *   id = "oauth2_connect_widget",
 *   label = "OAuth2 connection widget",
 *   field_types = {
 *     "oauth2"
 *   }
 * )
 */
class OAuth2ConnectWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $keyValueAccessState;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, array $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, KeyValueStoreExpirableInterface $key_value_access_state, Request $current_request) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->keyValueAccessState = $key_value_access_state;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('oauth2.key_value.access_state'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += [
      '#type' => 'details',
      '#title' => $this->fieldDefinition->getLabel(),
      '#open' => TRUE
    ];

    if ($items->getEntity()->isNew()) {
      $element['text'] = [
        '#markup' => $this->t('OAuth2 connect widget is not supported on unsaved entities.')
      ];
    }
    else {
      $field_name = $this->fieldDefinition->getName();

      $token = NULL;
      if (!$items[$delta]->isEmpty()) {
        $token = $items[$delta]->get('access_token')->getValue();
      }
      else {
        $code = $this->currentRequest->get('code');
        $state = $this->currentRequest->get('state');
        if ($code && $state && $this->keyValueAccessState->has($state)) {
          $token = $this->getProvider()->getAccessToken($code);
          $items[$delta]->setValue($token);
          $items->getEntity()->save();
        }
      }

      if ($items[$delta]->isEmpty()) {
        $state_data = [
          'entity_type_id' => $items->getEntity()->getEntityTypeId(),
          'entity_id' => $items->getEntity()->id(),
          'field_name' => $items->getName(),
          'delta' => $delta
        ];
        $element['authorize'] = $this->generateAuthorizeElement($state_data);
      }
      elseif (isset($token)) {
        $wrapper_id = Html::getUniqueId(implode('-', [
          $field_name,
          $delta,
          'wrapper'
        ]));
        $element['#prefix'] = "<div id={$wrapper_id}>";
        $element['#suffix'] = '</div>';

        $element['access_token'] = [
          '#type' => 'item',
          '#value' => $token,
          '#title' => $this->t('Access token'),
          '#markup' => $token->getToken()
        ];
        $element['unauthorize'] = [
          '#type' => 'submit',
          '#value' => $this->t('Unauthorize this access token.'),
          '#ajax' => [
            'callback' => [$this, 'unauthorizeAjax'],
            'wrapper' => $wrapper_id
          ],
          '#submit' => [[$this, 'unauthorizeSubmit']]
        ];
      }
    }

    return $element;
  }

  /**
   * Helper function to get the provider plugin for this field item.
   *
   * @return \Drupal\oauth2c\ProviderInterface|null
   */
  protected function getProvider() {
    return ProviderManager::getService()
      ->getProvider($this->getFieldStorageSetting('provider_id'));
  }

  /**
   * Helper function to get a specific field storage settings item
   * @param string|int|array $key
   * @return mixed
   */
  protected function getFieldStorageSetting($key) {
    if (!is_array($key)) {
      $key = [$key];
    }
    $settings = $this->getFieldStorageSettings();

    return NestedArray::getValue($settings, $key);
  }

  /**
   * Helper method to get the field storage definition settings.
   * @return \mixed[]
   */
  protected function getFieldStorageSettings() {
    return $this->getFieldStorageDefinition()->getSettings();
  }

  /**
   * Helper function to get the field storage definition object.
   *
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface
   */
  protected function getFieldStorageDefinition() {
    return $this->fieldDefinition->getFieldStorageDefinition();
  }

  protected function generateAuthorizeElement(array $state = []) {
    $provider = $this->getProviderEntity();
    $url_options = [
      'attributes' => [
        'target' => 'blank'
      ],
      'state' => $state
    ];
    $url = $provider->toUrl('authorize', $url_options);
    $element['authorize'] = Link::fromTextAndUrl('Authorize', $url)
      ->toRenderable();
    return Link::fromTextAndUrl('Authorize', $url)->toRenderable();
  }

  /**
   * @return \Drupal\oauth2c\ProviderEntityInterface|null
   */
  protected function getProviderEntity() {
    if ($provider_id = $this->getFieldStorageSetting('provider_id')) {
      return ProviderEntity::load($provider_id);
    }
    return NULL;
  }

  /**
   * This submission function is called by the unauthorize submit element.
   * It will remove the field item relating to that element, and save the entity.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function unauthorizeSubmit(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    $delta = NestedArray::getValue($form, array_merge(array_slice($button['#array_parents'], 0, -1), ['#delta']));
    $field_name = NestedArray::getValue($form, array_merge(array_slice($button['#array_parents'], 0, -2), ['#field_name']));

    $form_object = $form_state->getFormObject();
    if ($form_object instanceof EntityFormInterface) {
      $entity = $form_object->getEntity();
      if ($entity instanceof FieldableEntityInterface) {
        $entity->get($field_name)->removeItem($delta);
        $entity->save();
      }
    }
  }

  /**
   * Removes the access_token & unauthorize HTML elements and adds the authorize
   * element instead.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return array
   */
  public function unauthorizeAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widget item container.
    $item_element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));

    // Go one level up in the form, to the widget item container.
    $field_element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));

    unset($item_element['access_token']);
    unset($item_element['unauthorize']);

    $state_data = [
      'entity_type_id' => $form_state->getFormObject()
        ->getEntity()
        ->getEntityTypeId(),
      'entity_id' => $form_state->getFormObject()->getEntity()->id(),
      'field_name' => $field_element['#field_name'],
      'delta' => $item_element['#delta']
    ];

    $item_element['authorize'] = $this->generateAuthorizeElement($state_data);

    return $item_element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return array_map(function ($value) {
      return isset($value['access_token']) ? $value['access_token'] : NULL;
    }, $values);
  }
}