<?php


namespace Drupal\oauth2c\Plugin\Field\FieldType;


use Drupal\Core\Field\FieldException;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\oauth2c\ProviderManager;
use Drupal\oauth2c\ResourceDefinition;
use League\OAuth2\Client\Token\AccessToken;

/**
 * @FieldType(
 *   id = "oauth2",
 *   label = @Translation("OAuth2 connection field"),
 *   default_formatter = "oauth2_formatter",
 *   default_widget = "oauth2_connect_widget",
 *   category = @Translation("OAuth 2")
 * )
 */
class OAuth2Item extends FieldItemBase {

  /**
   * @var \Drupal\oauth2c\ProviderInterface
   */
  protected $provider;

  public static function defaultStorageSettings() {
    return [
      'provider_id' => NULL
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'access_token';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $storage_definition) {
    $properties_definitions = [];

    $provider_id = $storage_definition->getSetting('provider_id');

    $properties_definitions['access_token'] = DataDefinition::create('oauth2_access_token')
      ->setSetting('provider_id', $provider_id)
      ->setRequired(TRUE)
      ->setLabel(t('Access token'));

    if ($provider_id) {
      $provider = static::doGetProvider($provider_id);
      $type = 'oauth2_resource:' . $provider->getPluginDefinition()['resource_owner'];

      $properties_definitions['resource_owner'] = ResourceDefinition::createFromDataType($type)
        ->setLabel('Resource owner')
        ->setSetting('provider_id', $provider_id);
    }
    else {
      $properties_definitions['resource_owner'] = ResourceDefinition::create()
        ->setLabel('Resource owner');
    }

    return $properties_definitions;
  }

  /**
   * The OAuth2 client provider plugin.
   *
   * @param string $provider_id
   * @return \Drupal\oauth2c\ProviderInterface|null
   */
  protected static function doGetProvider($provider_id) {
    return ProviderManager::getService()->getProvider($provider_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'access_token' => [
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ]
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['provider_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('OAuth 2 client provider entity'),
      '#description' => $this->t('The provider entity describes the connection to the OAuth 2 server.'),
      '#target_type' => 'oauth2_provider',
      '#default_value' => $this->getProvider(),
      '#required' => TRUE
    ];

    return $element;
  }

  /**
   * The OAuth2 client provider plugin.
   *
   * @return \Drupal\oauth2c\ProviderInterface|null
   */
  public function getProvider() {
    if (empty($this->provider) && $provider_id = $this->getStorageSetting('provider_id')) {
      $this->provider = static::doGetProvider($provider_id);
    }
    return $this->provider;
  }

  protected function getStorageSetting($key) {
    return $this->getStorageDefinition()->getSetting($key);
  }

  protected function getStorageDefinition() {
    return $this->getFieldDefinition()->getFieldStorageDefinition();
  }

  public function setValue($values, $notify = TRUE) {
    if (is_array($values) && isset($values['access_token'])) {
      $value = $values['access_token'];
    }
    else {
      $value = $values;
    }

    if ($value instanceof AccessToken) {
      if ($value->hasExpired()) {
        $value = $this->getProvider()->refreshToken($value);
      }
      $this->set('access_token', $value);
    }
    elseif (is_null($value)) {
      $this->applyDefaultValue($notify);
    }
    else {
      throw new FieldException('The received value (' . print_r($value, TRUE) . ') is not (and cannot be converted to) ' . AccessToken::class);
    }
  }

  /**
   * @return \Drupal\oauth2c\Plugin\DataType\AccessTokenData
   */
  public function getAccessToken() {
    return $this->get('access_token');
  }

  protected function writePropertyValue($property_name, $value) {
    parent::writePropertyValue($property_name, $value);
    if ($property_name === 'access_token' && empty($this->get('resource_owner')
        ->getValue())
    ) {
      $this->set('resource_owner', $this->getProvider()
        ->getResourceOwner($value));
      $this->getResourceOwner()->setAccessToken($value);
    }
  }

  /**
   * @return \Drupal\oauth2c\Plugin\DataType\ResourceData
   */
  public function getResourceOwner() {
    return $this->get('resource_owner');
  }

  protected function getStorageSettings() {
    return $this->getStorageDefinition()->getSettings();
  }
}