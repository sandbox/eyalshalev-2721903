<?php


namespace Drupal\oauth2c\Plugin\Field\FieldFormatter;


use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\oauth2c\ProviderManager;
use Drupal\oauth2c_bitbucket\Plugin\Oauth2\Client\Provider\BitbucketProvider;

/**
 * @FieldFormatter(
 *   id = "oauth2_formatter",
 *   label = @Translation("OAuth2"),
 *   field_types = {
 *     "oauth2"
 *   }
 * )
 */
class OAuth2Formatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $render = [];

    /** @var FieldItemInterface $item */
    foreach ($items as $delta => $item) {
      if (!$item->isEmpty()) {

        /** @var BitbucketProvider $provider */
        $provider = ProviderManager::getService()
          ->getProvider($this->getFieldSetting('provider_id'));
        if ($item->get('value')->getValue()->hasExpired()) {
          $item->setValue($provider->refreshToken($item->get('value')
            ->getValue()), TRUE);
          $items->getEntity()->save();
        }

        $render[$delta] = [
          '#type' => 'oauth2_access_token',
          '#token' => $item->get('value')->getValue()
        ];
      }
    }

    return $render;
  }
}