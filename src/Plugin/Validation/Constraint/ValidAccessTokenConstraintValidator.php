<?php


namespace Drupal\oauth2c\Plugin\Validation\Constraint;


use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ValidAccessTokenConstraintValidator extends ConstraintValidator {


  /**
   * {@inheritdoc}
   * @param mixed $value
   * @param ValidAccessTokenConstraint $constraint
   */
  public function validate($value, Constraint $constraint) {
    if (!is_null($value) && !($value instanceof AccessToken)) {
      $this->context->addViolation($constraint->message, [
        '%value' => print_r($value, TRUE)
      ]);
    }
  }
}