<?php


namespace Drupal\oauth2c\Plugin\Validation\Constraint;


use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Validator\Constraint;

/**
 * @Constraint(
 *   id = "ValidAccessToken",
 *   label = @Translation("Valid access token", context = "Validation"),
 *   type = "oauth2_access_token"
 * )
 */
class ValidAccessTokenConstraint extends Constraint {
  public $message = 'oauth2_access_token data must be an instance of ' . AccessToken::class . ', %value received';
}