<?php


namespace Drupal\oauth2c;


use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class ProviderEntityListBuilder extends ConfigEntityListBuilder {

  public function getDefaultOperations(EntityInterface $entity) {
    
    $operations = [];
    
    return $operations + parent::getDefaultOperations($entity);
  }

  public function getOperations(EntityInterface $entity) {
    return array_map(function(array $operation) {
      $operation['url']->setRouteParameter('destination', \Drupal::request()->getRequestUri());
      return $operation;
    }, parent::getOperations($entity));
  }

  public function buildHeader() {
    $header = [];

    $header['id'] = $this->t('ID');

    return $header + parent::buildHeader();
  }

  public function buildRow(EntityInterface $entity) {

    $row = [];

    $row['id'] = $entity->id();

    return $row + parent::buildRow($entity);
  }
}