<?php


namespace Drupal\oauth2c;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface ProviderEntityInterface extends ConfigEntityInterface {

  /**
   * @return \Drupal\oauth2c\ProviderInterface
   */
  public function getProvider();
}