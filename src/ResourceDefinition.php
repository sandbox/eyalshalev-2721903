<?php


namespace Drupal\oauth2c;


use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\TraversableTypedDataInterface;

class ResourceDefinition extends ComplexDataDefinitionBase {

  const TYPE = 'oauth2_resource';

  /**
   * @var \Drupal\oauth2c\ResourceTypeInterface
   */
  protected $resourceType;

  public function __construct(array $values) {
    // Resources are always computed.
    $values['computed'] = TRUE;
    parent::__construct($values);
  }

  public static function createFromDataType($type) {
    $parts = explode(':', $type);
    if ($parts[0] != static::TYPE) {
      throw new \InvalidArgumentException('Data type must be in the form of "' . static::TYPE . ':RESOURCE_TYPE."');
    }
    $definition = static::create();
    // Set the passed entity type and bundle.
    if (isset($parts[1])) {
      $definition->setResourceTypeId($parts[1]);
    }
    return $definition;
  }

  /**
   * {@inheritdoc}
   * @return static
   */
  public static function create($type = self::TYPE) {
    return parent::create($type);
  }

  /**
   * Sets the resource type.
   * @param string $type
   * @return $this
   */
  public function setResourceTypeId($type) {
    if (ResourceTypeManager::getService()->hasDefinition($type)) {
      $this->definition['resource_type'] = $type;
    }
    else {
      $this->definition['resource_type'] = ResourceTypeManager::getService()
        ->getDefaultPluginId();
    }
    return $this;
  }

  /**
   * @param $resource
   * @param \Drupal\Core\TypedData\TraversableTypedDataInterface|NULL $parent
   * @return \Drupal\Core\TypedData\TypedDataInterface[]
   */
  public function getProperties($resource, TraversableTypedDataInterface $parent = NULL) {
    return $this->getResourceType()->getProperties($resource, $parent);
  }

  /**
   * @return \Drupal\oauth2c\ResourceTypeInterface
   */
  public function getResourceType() {
    if (empty($this->resourceType)) {
      $this->resourceType = ResourceTypeManager::getService()
        ->getInstance(['plugin_id' => $this->getResourceTypeId()]);
    }
    return $this->resourceType;
  }

  /**
   * @return string
   */
  public function getResourceTypeId() {
    return $this->definition['resource_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {

    if (empty($this->propertyDefinitions)) {
      $this->propertyDefinitions = $this->getResourceType()
        ->getPropertyDefinitions();
    }

    return $this->propertyDefinitions;
  }

  /**
   * {@inheritdoc}
   */
  public function setComputed($computed) {
    $this->logger()->warning('OAuth2 resources are always computed.');
    return $this;
  }

  /**
   * @return \Psr\Log\LoggerInterface
   */
  protected function logger() {
    return \Drupal::logger('oauth2c.resource');
  }
}