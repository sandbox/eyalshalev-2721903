<?php


namespace Drupal\oauth2c;


use Drupal\Component\Plugin\PluginManagerInterface;

interface ResourceTypeManagerInterface extends PluginManagerInterface {

}