<?php


namespace oauth2c\src;


use Drupal\Core\TypedData\TypedData;

abstract class LazyData extends TypedData {

  /**
   * @var \callable
   */
  protected $callback;

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    if (is_callable($value)) {
      $this->callback = $value;
    }
    elseif (is_null($value)) {
      $this->doSetValue($value, $notify);
    }
    else {
      throw new \BadMethodCallException('LazyData values must be callables.');
    }
  }

  /**
   * Called by getValue (& setValue when value is null) with the value returned by
   * the callback.
   *
   * @param mixed $value
   * @param bool $notify
   * @return mixed
   */
  public function doSetValue($value, $notify = TRUE) {
    parent::setValue($value, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if (empty(parent::getValue())) {
      $this->doSetValue(call_user_func_array($this->callback, [$this]));
    }

    return parent::getValue();
  }
}