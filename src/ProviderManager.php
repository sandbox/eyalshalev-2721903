<?php

namespace Drupal\oauth2c;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\oauth2c\Annotation\OAuth2Provider as OAuth2ClientProviderAnnotation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OAuth2ClientProviderManager.
 *
 * @package Drupal\oauth2c
 */
class ProviderManager extends DefaultPluginManager implements ProviderManagerInterface {

  const SERVICE_NAME = 'oauth2.client.provider.manager';

  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $storage;

  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityTypeInterface
   */
  protected $entityType;

  protected $prefix;

  /**
   * OAuth2ClientProviderManager constructor.
   * @param \Traversable $namespaces
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct('Plugin/OAuth2/Client/Provider', $namespaces, $module_handler, ProviderInterface::class, OAuth2ClientProviderAnnotation::class);
    $this->storage = $entity_type_manager->getStorage('oauth2_provider');
    $this->entityType = $entity_type_manager->getDefinition('oauth2_provider');
    $this->prefix = $this->entityType->getConfigPrefix();
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|NULL $container
   * @return static
   */
  public static function getService(ContainerInterface $container = NULL) {
    $container = $container ? : \Drupal::getContainer();
    return $container->get(static::SERVICE_NAME);
  }

  /**
   * @param string $plugin_id
   * @param array $configuration
   * @return \Drupal\oauth2c\ProviderInterface
   */
  public function createInstance($plugin_id, array $configuration = array()) {
    return parent::createInstance($plugin_id, $configuration);
  }

  /**
   * @param array $options
   *   Must contain the following properties:
   *    - provider_id: The id of the service provider plugin.
   *    - service_name: The machine name of the service provider instance.
   * @return false|\Drupal\oauth2c\ProviderInterface
   */
  public function getInstance(array $options) {
    if (isset($options['provider_id']) && isset($options['service_name'])) {
      $id = $this->generateId($options['provider_id'], $options['service_name']);
      return $this->getProvider($id);
    }
    throw new \InvalidArgumentException('The getInstance method of ServiceProviderManager must include provider_id & service_name keys.');
  }

  /**
   * Helper method to generate an id of a service provider based on the provider
   * id and the service name.
   *
   * @param string $provider_id
   * @param string $service_name
   * @return string
   */
  private function generateId($provider_id, $service_name) {
    return implode('.', [$this->prefix, $provider_id, $service_name]);
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider($provider_id) {
    $provider_entity = $this->storage->load($provider_id);
    if ($provider_entity instanceof ProviderEntityInterface) {
      return $provider_entity->getProvider();
    }
    else {
      return NULL;
    }
  }
}
