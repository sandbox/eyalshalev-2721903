<?php


namespace Drupal\oauth2c;


use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\TypedData\DataDefinition;

abstract class ResourceTypeBase extends PluginBase implements ResourceTypeInterface {

  /**
   * @var \Drupal\Core\TypedData\DataDefinition[]
   */
  protected $definitions = [];

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (empty($this->definitions)) {
      $this->definitions = array_map(function (DataDefinition $property) {
        // Resource properties are always computed.
        return $property->setComputed(TRUE);
      }, $this->doGetPropertiesDefinitions());
    }
    return $this->definitions;
  }

  /**
   * @return \Drupal\Core\TypedData\DataDefinition[]
   */
  abstract protected function doGetPropertiesDefinitions();
}