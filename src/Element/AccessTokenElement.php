<?php


namespace Drupal\oauth2c\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\Element\Container;
use League\OAuth2\Client\Token\AccessToken;

/**
 * @RenderElement("oauth2_access_token")
 */
class AccessTokenElement extends Container {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return NestedArray::mergeDeep(parent::getInfo(), [
      '#pre_render' => [
        [$this, 'preRender']
      ],
      '#token' => NULL,
    ]);
  }

  /**
   * @param array $element
   * @return array
   */
  public function preRender(array &$element) {
    if (!isset($element['#token'])) {
      throw new \BadMethodCallException('The #token item is not found in the $element array.');
    }
    else {
      $token = $element['#token'];
      if ($token instanceof AccessToken) {

        $element['access_token'] = [
          '#type' => 'item',
          '#value' => $token->getToken(),
          '#markup' => $token->getToken(),
          '#title' => $this->t('Access token')
        ];

        $element['refresh_token'] = [
          '#type' => 'item',
          '#value' => $token->getRefreshToken(),
          '#markup' => $token->getRefreshToken(),
          '#title' => $this->t('Refresh token')
        ];


        $element['expires'] = [
          '#type' => 'item',
          '#value' => $token->getExpires(),
          '#markup' => \Drupal::service('date.formatter')->format($token->getExpires()),
          '#title' => $this->t('Token expiration date.')
        ];

        if ($resource_owner_id = $token->getResourceOwnerId()) {
          $element['resource_owner_id'] = [
            '#type' => 'item',
            '#value' => $resource_owner_id,
            '#markup' => $resource_owner_id,
            '#title' => $this->t('Resource owner id')
          ];
        }

        return $element;
      }
      else {
        throw new \BadMethodCallException('The #token item is not an instance of ' . AccessToken::class);
      }
    }
  }
}