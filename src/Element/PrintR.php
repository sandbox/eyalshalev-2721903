<?php


namespace Drupal\oauth2c\Element;


use Drupal\Core\Render\Element\InlineTemplate;

/**
 * @RenderElement("print_r")
 */
class PrintR extends InlineTemplate {
  /**
   * @inheritDoc
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#template'] = '<pre>{{ value }}</pre>';
    $info['#context'] = ['value' => NULL];
    $info['#value'] = NULL;
    $info['#pre_render'] = array_merge(
      [[$this, 'preRender']],
      $info['#pre_render']
    );

    return $info;
  }

  public function preRender($element) {
    $element['#context']['value'] = print_r($element['#value'], TRUE);
    return $element;
  }

}