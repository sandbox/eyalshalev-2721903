<?php


namespace Drupal\oauth2c;


use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\oauth2c\Plugin\DataType\ResourceData;

interface ResourceTypeInterface extends PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * @return \Drupal\Core\TypedData\DataDefinitionInterface[]
   */
  public function getPropertyDefinitions();

  /**
   * The TypedData properties of the raw $resource object (or array).
   * @param object|array $resource
   * @param \Drupal\oauth2c\Plugin\DataType\ResourceData $resource_property
   * @return \Drupal\Core\TypedData\TypedDataInterface[]
   */
  public function getProperties($resource, ResourceData $resource_property);
}