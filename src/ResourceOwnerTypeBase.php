<?php


namespace Drupal\oauth2c;


use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\oauth2c\Plugin\DataType\ResourceData;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

abstract class ResourceOwnerTypeBase extends ResourceTypeBase {

  protected $definitions;

  /**
   * {@inheritdoc}
   */
  public function doGetPropertiesDefinitions() {
    return [
      'id' => DataDefinition::create('string'),
      'data' => MapDataDefinition::create()
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties($resource, ResourceData $resource_property) {
    if ($resource instanceof ResourceOwnerInterface) {
      $definitions = $this->getPropertyDefinitions();
      $values = $this->getValues($resource, $resource_property);

      $properties = [];

      /**
       * @var string $key
       * @var DataDefinition $definition
       */
      foreach ($definitions as $key => $definition) {
        $data_class = $definition->getClass();
        $properties[$key] = call_user_func_array([
          $data_class,
          'createInstance'
        ], [$definition, $key, $resource_property]);
        $properties[$key]->setValue($values[$key]);
      }

      return $properties;
    }
    else {
      throw new \InvalidArgumentException('The resource object of resource owner data must implement the ' . ResourceOwnerInterface::class . ' interface.');
    }
  }

  /**
   * @param $resource
   * @param \Drupal\oauth2c\Plugin\DataType\ResourceData $property
   * @return array
   */
  public function getValues($resource, ResourceData $property) {
    if ($resource instanceof ResourceOwnerInterface) {
      return [
        'id' => $resource->getId(),
        'data' => $resource->toArray(),
      ];
    }
    else {
      throw new \InvalidArgumentException('The resource object of resource owner data must implement the ' . ResourceOwnerInterface::class . ' interface.');
    }
  }
}